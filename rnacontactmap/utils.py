"""Utilities module for rnacontactmap API endpoints and methods.
"""
import logging
import os

import subprocess  # nosec B404
import tempfile

import numpy as np
from rnacontactmap import config
import plotly.graph_objects as go
from plotly.subplots import make_subplots


logger = logging.getLogger("__name__")


def ls_local(head_task: str):
    """Utility to return a list of current models stored in the local folders
    configured for cache.

    Arguments:
        head_task -- String with the head task section in settings.

    Returns:
        A list of strings in the format {head_task}_{timestamp}.
    """
    logger.debug("Scanning at: %s", config.MODEL_CACHES[head_task])
    dirscan = os.scandir(config.MODEL_CACHES[head_task])
    return [entry.name for entry in dirscan if entry.is_dir()]


def ls_remote(head_task: str):
    """Utility to return a list of current backbone models stored in the
    remote folder configured in the backbone url.

    Arguments:
        head_task -- String with the head task section in settings.

    Returns:
        A list of strings in the format {head_task}_{timestamp}.
    """
    logger.debug(
        "path to remote model: %s", config.MODEL_REMOTES[head_task]
    )
    remote_dir = config.MODEL_REMOTES[head_task]
    return list_directories_with_rclone("rshare", remote_dir)


def list_directories_with_rclone(remote_name, directory_path):
    """
    Function to list directories within a given directory in Nextcloud
    using rclone.

    Args:
        remote_name (str): Name of the configured Nextcloud remote in rclone.
        directory_path (str): Path of the parent directory to list the
            directories from.

    Returns:
        list: List of directory names within the specified parent directory.
    """
    command = ["rclone", "lsf", remote_name + ":" + directory_path]
    result = subprocess.run(
        command, capture_output=True, text=True, shell=False
    )  # nosec B603

    if result.returncode == 0:
        directory_names = result.stdout.splitlines()
        directory_names = [d.rstrip("/") for d in directory_names]
        return directory_names
    else:
        print("Error executing rclone command:", result.stderr)
        return []


def download_models(models_dict: dict):
    """Downloads indicated sub/models from a key `head_task` remote directory
    where:
        head_task -- String with the head task section in settings.
        submodel -- String containing the submodel to search.

    Arguments:
        models_dict -- Dict as {head_task: [submodel]} for download_model.

    Raises:
        HTTPUnprocessableEntity: Submodel not available in head task remote.
    """
    for head_task, submodels in models_dict.items():
        logger.debug(
            "Head task: %d and submodels: %s", head_task, submodels
        )
        submodel = models_dict[head_task]
        local_path = config.MODEL_CACHES[head_task]
        ckpt_path = os.path.join(local_path, submodel)
        remote = config.MODEL_REMOTES[head_task]
        if submodel not in os.listdir(local_path):
            print("downloading the chekpoint from nextcloud ...")
            model_path = os.path.join(remote, submodel)
            download_directory_with_rclone(
                "rshare", model_path, ckpt_path
            )
            if "final_model.ckpt" not in os.listdir(ckpt_path):
                raise Exception(
                    f"No files were copied to {ckpt_path}"
                )

            print(
                f"The model for {submodel} was copied to {local_path}"
            )
        else:
            print(
                f"Skipping download for {submodel} as the model"
                f"  already exists in {ckpt_path}"
            )


def download_directory_with_rclone(
    remote_name, remote_directory, local_directory
):
    """
    Function to download a directory using rclone.

    Args:
        remote_name (str): Name of the configured remote in rclone.
        remote_directory (str): Path of the remote directory to be downloaded.
        local_directory (str): Path of the local directory to save the
        downloaded files.

    Returns:
        None
    """

    command = [
        "rclone",
        "copy",
        remote_name + ":" + remote_directory,
        local_directory,
    ]
    result = subprocess.run(
        command, capture_output=True, text=True, shell=False
    )  # nosec B603

    if result.returncode == 0:
        print("Directory downloaded successfully.")
    else:
        print("Error executing rclone command:", result.stderr)


def convert_boolean_to_binary(data):
    if isinstance(data, bool):
        return int(data)
    elif isinstance(data, list):
        return [convert_boolean_to_binary(item) for item in data]
    elif isinstance(data, dict):
        return {
            key: convert_boolean_to_binary(value)
            for key, value in data.items()
        }
    else:
        return data


def plotly_contact_maps(filename, list_dict):
    """
    Receive a prediction dictionary and return an RNA contact map plot
    for each input file, as well as an interactive HTML file that
    includes all of these plots.

    Args:
        filename (str): name of the input file
        list_dict: a list of length 2 containing two dictionaries.
        The keys of these dictionaries are equal to the input
        file names, and the values are equal to the prediction L*L
        array and binary prediction array, respectively.

    Returns:
        fig: contains a Plotly graph objects that show the heat
        map for each input file.
        new_name:  the path to the interactive HTML file that
        contains this Plotly graph objects.

    """
    n = len(list_dict[0])

    title_tuple = tuple(["Prediction", "Prediction (binary)"] * n)

    fig = make_subplots(
        rows=n, cols=2, subplot_titles=title_tuple, print_grid=True
    )

    sub_title = []

    for i, key in enumerate(list_dict[0]):
        preds_shaped = np.array(list(list_dict[0][key])).squeeze()
        preds_shaped_binary = np.array(
            list(list_dict[1][key])
        ).squeeze()

        preds_shaped_binary = preds_shaped_binary.astype(int)

        ferq = np.arange(0, len(preds_shaped)).tolist()
        chart1 = go.Heatmap(
            z=preds_shaped,
            x=ferq,
            y=ferq,
            coloraxis=f"coloraxis{i+1}",
            name=f" {key.split('/')[-1]}",
        )
        chart2 = go.Heatmap(
            z=preds_shaped_binary,
            x=ferq,
            y=ferq,
            coloraxis=f"coloraxis{i+1}",
            name=f" {filename}",
        )

        fig.add_trace(chart1, row=i + 1, col=1)
        fig.add_trace(chart2, row=i + 1, col=2)

        # save the subplot title to update later
        sub_title.extend(
            [
                f"Prediction: {filename}",
                f"Prediction (binary): {filename}",
            ]
        )

    # creating colorbar for each row
    coloraxis_dicts = {}
    j = 1
    for i in range(1, n * 2, 2):
        yaxis_name = f"yaxis{i}"
        yaxis_domain = fig.layout[
            yaxis_name
        ].domain  # y coordinate of each plot
        colorbar_len = (
            yaxis_domain[1] - yaxis_domain[0]
        )  # hight of the colorbar==hight of the plot
        colorbar_y = yaxis_domain[0] + colorbar_len / 2
        coloraxis_name = f"coloraxis{j}"
        coloraxis_dicts[coloraxis_name] = dict(
            #  colorscale='matter_r',
            colorbar_y=colorbar_y,
            colorbar_len=colorbar_len,
            colorbar_thickness=30,
        )
        j += 1

    fig.update_layout(
        title="Inference Data",
        title_x=0.5,
        height=600 * len(list_dict[0]),
        **coloraxis_dicts,
    )  # Set the title position to the center of the plot)

    # Add subtitle to each subplot
    fig.for_each_annotation(lambda a: a.update(text=sub_title.pop(0)))
    # save fig in HTML file
    with tempfile.NamedTemporaryFile(
        mode="w", delete=False, suffix=".html"
    ) as f:
        # config for Html output file.
        config = {
            "scrollZoom": True,
            "toImageButtonOptions": {
                "format": "svg",
                "filename": "rnacontactmap",
            },
        }

        fig.write_html(
            f.name,
            config=config,
            default_width=1100,
            default_height=1000,
            animation_opts=True,
        )
        current_name = f.name
        new_name = "plot_contactmaps.html"
        os.rename(current_name, new_name)
        message = open(new_name, "rb")

        return message


if __name__ == "__main__":
    download_models(
        models_dict={"backbone": "inpainting_2023-02-10_181229"}
    )
