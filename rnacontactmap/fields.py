"""Module for defining custom web fields to use on the API interface.
"""
from webargs import fields, validate
from marshmallow import Schema


class PredictArgsSchema(Schema):
    class Meta:
        ordered = True

    input_file = fields.Field(
        required=True,
        type="file",
        location="form",
        metadata={
            "description": "MAS file to train model or predict contact map."
        },
    )
    backbone = fields.Str(
        required=False,
        load_default=None,
        metadata={
            "description": "Backbone used for prediction ({HeadTask}_{Timestamp})."
            " To see the list of checkpoints at local and remote, please execute the metadata method."
        },
    )

    xgboost = fields.Str(
        required=False,
        load_default=None,
        metadata={
            "description": "XGBoost used for prediction (XGBoost}_{Timestamp})."
            " To see the list of checkpoints at local and remote, please execute the metadata method."
        },
    )

    downstream_task = fields.Str(
        required=False,
        load_default="contact",
        validate=validate.OneOf(["contact", " thermostable"]),
        metadata={
            "description": "Downstream task (contact, thermostable)"
            
        },
    )
    subsampling_mode = fields.Str(
        required=False,
        load_default="uniform",
        validate=validate.OneOf(["uniform", "diversity", "fixed"]),
        metadata={
            "description": "Subsampling mode used for prediction. "
            
        },
    )

    distance_threshold = fields.Float(
        required=False,
        load_default=10.0,
        metadata={
            "description": "Minimum distance between two atoms in angström"
            " that is not considered as a contact.\n"
            "minimum: 0\n"
            "maximum: 20"
        },
    )

    diag_shift = fields.Int(
        required=False,
        load_default=4,
        metadata={
            "description": "Width of the area around the main"
            " diagonal of prediction maps that is ignored."
        },
    )

    use_gpu = fields.Bool(
        required=False,
        load_default=False,
        metadata={
            "description": "Flag to enable prediction calculations"
            " with available GPU and CUDA resources."
        },
    )

    accept = fields.Str(
        required=False,
        load_default="application/json",
        validate=validate.OneOf(
            ["application/json", "application/pdf", "text/html"]
        ),
        metadata={
            "description": "Return format for RNA contactmap prediction plots."
        },
    )
