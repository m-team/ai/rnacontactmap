"""Functions to integrate rnacontactmap model with the DEEPaaS API.

For more information about how to edit the module see, take a look at the
docs [1] and at a canonical exemplar module [2].

[1]: https://docs.deep-hybrid-datacloud.eu/
[2]: https://github.com/deephdc/demo_app
"""

import logging

from rnacontactmap import config, fields, utils, parsers
from rnacontactmap.scripts import inference_Xgb_downstream

logger = logging.getLogger("__name__")


def get_metadata():
    """Returns a dictionary containing metadata information about the module.

    Returns:
        A dictionary containing metadata information required by DEEPaaS.
    """
    metadata = {
        "authors": config.API_METADATA.get("authors"),
        "author-email": config.API_METADATA.get("author-emails"),
        "description": config.API_METADATA.get("summary"),
        "license": config.API_METADATA.get("license"),
        "version": config.API_METADATA.get("version"),
        "backbone_local": utils.ls_local("backbone"),
        "backbone_remote": utils.ls_remote("backbone"),
        "xgboost_local": utils.ls_local("xgboost"),
        "xgboost_remote": utils.ls_remote("xgboost"),
    }
    logger.debug("Package model metadata: %d", metadata)
    return metadata


def get_predict_args():
    """Return the arguments that are needed to perform a prediction.

    Returns:
        Dictionary of webargs fields.
    """
    predict_args = fields.PredictArgsSchema().fields
    logger.debug("Web arguments: %s", predict_args)
    return predict_args


def predict(input_file, backbone, xgboost, accept, **kwds):
    """Performs RNA contact map prediction on a given input data.

    Args:
        input_file: Input data to use on the prediction of contact map.
        backbone: Backbone submodel version used in the prediction.
        xgboost: XGBoost submodel version used in the prediction.
        accept: Format required for the method output.
        **kwds: Arbitrary keyword arguments from get_predict_args.

    Returns:
        The predicted RNA contact map.
    """
    logger.debug("Data: %d, Backbone: %d", input_file, backbone)
    logger.debug("Configuration: %d", kwds)
    utils.download_models({"backbone": backbone, "xgboost": xgboost})
    filename = input_file.original_filename
    script = inference_Xgb_downstream.inference_XGB
    result = script(
        [input_file.filename],
        backbone,
        xgboost,
        batch_size=kwds.get("batch_size", 1),
        subsampling_mode=kwds.get("subsampling_mode", "uniform"),
        distance_threshold=kwds.get("distance_threshold", 10.0),
        diag_shift=kwds.get("diag_shift", 4),
        use_gpu=kwds.get("use_gpu", False),
        downstream_task=kwds.get("downstream_task", "contact"),
        original_filename=filename,
    )
    response_converter = parsers.ResponseConverter(filename, *result)
    return response_converter.response_parsers()[accept]()


if __name__ == "__main__":
    args = {
        "backbone": "inpainting_2023-02-10_181229",
        "xgboost": "inpainting_2023-02-10_181229",
        "input_file": "rna_alignment.fasta",
    }
