# RNA Contact Map Prediction

[![Build Status](https://jenkins.indigo-datacloud.eu/buildStatus/icon?job=Pipeline-as-code/DEEP-OC-org/UC-falibabaei-rnacontactmap/master)](https://jenkins.indigo-datacloud.eu/job/Pipeline-as-code/job/DEEP-OC-org/job/UC-falibabaei-rnacontactmap/job/master)

RNA, a vital category of biomolecules similar to proteins, plays essential roles in numerous fundamental
 processes. Despite being primarily recognized as a passive carrier of genetic information, RNA has been 
linked to numerous additional biological functions.

The model in this [existing codebase](https://github.com/KIT-MBS/selbstaufsicht), named BARNACLE, combines the utilization of unlabeled data through self-supervised pre-training  
and the efficient use of sparse labeled data through an XGBoost classifier, predicting spatial adjacencies ('contact maps') as a proxy for 3D structure.

The model was trained using the data from  [this repository](https://github.com/KIT-MBS/RNA-dataset/tree/master) [1] 

## Adding DeepaaS API into the existing codebase
In this repository, we have integrated a DeepaaS API into the [existing codebase](https://github.com/KIT-MBS/selbstaufsicht). 

To launch it, first install the package and prepare your environment then run [deepaas](https://github.com/indigo-dc/DEEPaaS) from the application client:
> Use editable mode `pip install -e .` when you are testing your patch to a package through another project.

```bash
git clone https://github.com/.../rnacontactmap  # Download repository
cd rnacontactmap  # Navigate inside rnacontactmap project
git submodule init
git submodule update --remote --merge
pip install -e ./path/to/submodule/dir
pip install -e .
deepaas-run --listen-ip 0.0.0.0  # You deploy using deepaas
```
><span style="color:Blue">**Note:**</span> Before installing the API and submodule requirements, please make sure to install the following system packages: `gcc`,  `libgl1` and libglib2.0-0 as well. These packages are essential for a smooth installation process and proper functioning of the framework.

```
apt update
apt install -y gcc
apt install -y libgl1
apt install -y libglib2.0-0
```
For more information about
Additionally you can configure the following environment variables for customization:

 - *MODEL_NAME* [**Optional**], name of the package model used to retrieving model metadata information.
    Default: `selbstaufsicht`.
 - *DATA_PATH* [**Optional**], local directory with data for training, testing or example files.
    Default: `./data`.
 - *BACKBONE_PATH* [**Optional**], local directory where to cache downloaded backbone models.
    Default: `./models/backbone`.
 - *XGBOOST_PATH* [**Optional**], local directory where to cache downloaded xgboost models.
    Default: `./models/xgboost`.
 - *REMOTE_URL* [**Optional**], path to the ckpts on a  remote storage sub/models.
    

The associated Docker container for this module can be found in https://github.com/falibabaei/DEEP-OC-rnacontactmap.

## Project structure

```
├── ChangeLog               <- Record of all notable changes made to a project
├── Jenkinsfile             <- Describes basic Jenkins CI/CD pipeline
├── LICENSE                 <- Project API license file
├── README.md               <- The top-level README for developers using this project.
├── data                    <- Folder to store data files (examples, testing, etc.)
│   └── RF00010.faclean         <- Dataset file example
├── models                  <- Folder to store local cached models
│   ├── backbone                <- Folder to store backbone models
│   │   └── {head_task}_{timestamp}     <- Backbone model folder example
│   │       ├── events.out...506642.0   <- Model checkpoint example
│   │       ├── final_checkpoint.ckpt   <- Model final checkpoint
│   │       └── hparams.yaml            <- Backbone model hyper parameters
│   └── xgboost                 <- Folder to store XGBoost models
│       └── {head_task}_{timestamp}     <- XGBoost model folder example
│           ├── model_checkpoint.json   <- Model checkpoint example
│           └── train_log.csv           <- Model training log
├── pyproject.toml          <- Makes project pip installable (pip install -e .)
├── requirements-test.txt   <- Requirements file for testing the service
├── requirements.txt        <- Requirements file for running the service
├── src                     <- Source folder containing the API code
│   └── rnacontactmap           <- Python RNA Contact Map API package
│       ├── VERSION                 <- File indicating the API/Package version
│       ├── config                  <- API configuration subpackage
│       ├── scripts                 <- API scripts subpackage for predictions
│       ├── __init__.py             <- File for initializing the python library
│       ├── api.py                  <- API core module for endpoint methods
│       ├── fields.py               <- API core fields for arguments
│       ├── parsers.py              <- API core for parsers and content types
│       └── utils.py                <- API utilities module
├── tests                   <- Folder containing tests for the API methods
│   ├── conftest.py             <- Module for test fixtures and parametrization
│   ├── test_metadata.py        <- Module for testing API get_metadata
│   └── test_predictions.py     <- Module for testing API predict
└── tox.ini                 <- Generic virtual environment configuration file
```

## Inference Methods

You can utilize the Swagger interface to upload a FASTA file, and obtain the following outputs:

- A JSON file containing the probability of the existence of connections at each point, as well as metadata regarding prediction and training components.
- A PDF file containing the contact map plot.
- An interactive HTML file displaying the contact map plot.

## References
[1] Taubert, O., von der Lehr, F., Bazarova, A. et al. RNA contact prediction by data efficient deep learning.,
        Commun Biol 6, 913 (2023). https://doi.org/10.1038/s42003-023-05244-9
