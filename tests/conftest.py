"""Module for evolution requirements fixtures"""
# pylint: disable=redefined-outer-name

import fnmatch
import os
from pathlib import Path

import pytest
from deepaas.model.v2.wrapper import UploadedFile

from rnacontactmap import api, config, parsers, utils


MODELS_BACKBONE = utils.ls_local("backbone")
MODELS_XGBOOST = utils.ls_local("xgboost")
DATA_FILES = os.listdir(config.DATA_PATH)


#####################################################
#  Fixtures to test metadata function
#####################################################


@pytest.fixture(scope="module")
def metadata():
    """Fixture to return defined api metadata."""
    return api.get_metadata()


@pytest.fixture(
    scope="module", params=fnmatch.filter(DATA_FILES, "*.faclean")
)
def input_file(request):
    """Fixture to return input_file argument to test."""
    file = str(Path(config.DATA_PATH) / request.param)
    content_type = "application/octet-stream"
    return UploadedFile(
        "input_file", file, content_type, request.param
    )


@pytest.fixture(scope="module", params=MODELS_BACKBONE)
def backbone(request):
    """Fixture to return backbone argument to test."""
    return request.param


@pytest.fixture(scope="module", params=MODELS_XGBOOST)
def xgboost(request):
    """Fixture to return XGBoosts argument to test."""
    return request.param


@pytest.fixture(scope="module", params=parsers.content_types)
def accept(request):
    """Fixture to return accept arguments to test."""
    return request.param


@pytest.fixture(scope="module", params=[1])
def batch_size(request):
    """Fixture to return batch sizes argument to test."""
    return request.param


@pytest.fixture(
    scope="module", params=["uniform", "diversity", "fixed"]
)
def sampling(request):
    """Fixture to return sampling argument to test."""
    return request.param


@pytest.fixture(scope="module", params=[10.0])
def distance(request):
    """Fixture to return distance argument to test."""
    return request.param


@pytest.fixture(scope="module", params=["contact"])
def downstream_task(request):
    """Fixture to return diagonal argument to test."""
    return request.param


@pytest.fixture(scope="module", params=[4])
def diagonal_shift(request):
    """Fixture to return diagonal argument to test."""
    return request.param


@pytest.fixture(scope="module", params=[False, True])
def gpu_flag(request):
    """Fixture to return gpu flag argument to test."""
    return request.param


@pytest.fixture(scope="module")
def pred_kwds(
    batch_size, sampling, distance, diagonal_shift, gpu_flag
):
    """Fixture to return arbitrary keyword arguments for predictions."""
    pred_kwds = {
        "batch_size": batch_size,
        "subsampling_mode": sampling,
        "distance_threshold": distance,
        "diag_shift": diagonal_shift,
        "use_gpu": gpu_flag,
    }
    return {k: v for k, v in pred_kwds.items() if v is not None}


@pytest.fixture(scope="module")
def predictions(input_file, backbone, xgboost, accept, pred_kwds):
    """Fixture to return predictions to assert properties."""
    return api.predict(
        input_file, backbone, xgboost, accept, **pred_kwds
    )
