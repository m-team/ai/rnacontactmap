"""Testing module for api metadata.
"""
# pylint: disable=redefined-outer-name
# pylint: disable=unused-argument


def test_authors(metadata):
    """Tests that metadata provides authors information."""
    assert "authors" in metadata

    assert isinstance(metadata["authors"], list)


def test_authors_emails(metadata):
    """Tests that metadata provides authors information."""
    assert "author-email" in metadata
    assert isinstance(metadata["authors"], list)


def test_description(metadata):
    """Tests that metadata provides description information."""
    assert "description" in metadata
    assert isinstance(metadata["description"], str)


def test_license(metadata):
    """Tests that metadata provides license information."""
    assert "license" in metadata
    assert isinstance(metadata["license"], str)


def test_version(metadata):
    """Tests that metadata provides version information."""
    assert "version" in metadata
    assert isinstance(metadata["version"], str)
    assert all(v.isnumeric() for v in metadata["version"].split("."))
    assert len(metadata["version"].split(".")) == 3


def test_backbone_local(metadata):
    """Tests that metadata provides backbone local information."""
    assert "backbone_local" in metadata
    assert isinstance(metadata["backbone_local"], list)


def test_backbone_remote(metadata):
    """Tests that metadata provides backbone remote information."""
    assert "backbone_remote" in metadata
    assert isinstance(metadata["backbone_remote"], list)


def test_xgboost_local(metadata):
    """Tests that metadata provides xgboost local information."""
    assert "xgboost_local" in metadata
    assert isinstance(metadata["xgboost_local"], list)


def test_xgboost_remote(metadata):
    """Tests that metadata provides xgboost remote information."""
    assert "xgboost_remote" in metadata
    assert isinstance(metadata["xgboost_remote"], list)
