"""Script to generate inference XGBoost Downstream predictions.
"""
from io import BytesIO
from pathlib import Path
from typing import List

import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
import torch
from fpdf import FPDF
from PyPDF3 import PdfFileMerger
import json

import xgboost as xgb
from rnacontactmap import config, api
from selbstaufsicht.models.xgb import xgb_utils


def save_contact_maps(
    preds: np.ndarray,
    msa_mapping: np.ndarray,
    msa_mask: np.ndarray,
    L_mapping: np.ndarray,
    file_names: List[str],
) -> None:
    """
    Saves predictions.

    Args:
        preds (np.ndarray): Predictions [B] as logits.
        msa_mapping (np.ndarray): Mapping: Data point -> MSA index [B].
        msa_mask (np.ndarray): MSA mask [B].
        L_mapping (np.ndarray): Mapping: MSA index -> MSA L.
        save_dir (str): Directory, where plots are saved.
        file_names (List[str]): File names for plots.
    """

    msa_indices = np.unique(msa_mapping)

    preds_ = np.full(len(msa_mask), -np.inf)
    if sum(msa_mask) != len(preds):
        raise ValueError("msa_mask and preds lengths do not match.")

    preds_[msa_mask] = preds

    preds_shaped_dict = {}
    preds_shaped_binary_dict = {}
    # for each MSA, save prediction
    for msa_idx in msa_indices:
        file_name = file_names[msa_idx].split(".")[0]

        mask = msa_mapping == msa_idx  # [B]
        L = L_mapping[msa_idx]

        preds_shaped = xgb_utils.sigmoid(preds_[mask].reshape((L, L)))
        preds_shaped += preds_shaped.T
        preds_shaped_binary = np.round(preds_shaped).astype(bool)
        preds_shaped_dict[file_name] = preds_shaped.tolist()

        preds_shaped_binary_dict[
            file_name
        ] = preds_shaped_binary.tolist()

    return preds_shaped_dict, preds_shaped_binary_dict


def plot_contact_maps_buffer(
    default_conf,
    preds: np.ndarray,
    msa_mapping: np.ndarray,
    msa_mask: np.ndarray,
    L_mapping: np.ndarray,
    file_names: List[str],
) -> BytesIO:
    """
    Plots predictions.

    Args:
        default_conf:
        preds (np.ndarray): Predictions [B] as logits.
        msa_mapping (np.ndarray): Mapping: Data point -> MSA index [B].
        msa_mask (np.ndarray): MSA mask [B].
        L_mapping (np.ndarray): Mapping: MSA index -> MSA L.
        file_names (List[str]): File names for plots.

    Returns:
        BytesIO: File object containing the plot.
    """

    msa_indices = np.unique(msa_mapping)

    preds_ = np.full(len(msa_mask), -np.inf)
    if sum(msa_mask) != len(preds):
        raise ValueError("msa_mask and preds lengths do not match.")
    preds_[msa_mask] = preds
    merger = PdfFileMerger()

    # for each MSA, plot prediction
    for msa_idx in msa_indices:
        file_name = file_names[msa_idx].split(".")[0]
        mask = msa_mapping == msa_idx  # [B]
        L = L_mapping[msa_idx]

        preds_shaped = xgb_utils.sigmoid(preds_[mask].reshape((L, L)))
        preds_shaped += preds_shaped.T
        preds_shaped_binary = np.round(preds_shaped).astype(bool)

        fig, ax = plt.subplots(1, 2)
        sns.heatmap(
            preds_shaped,
            fmt="",
            ax=ax[0],
            cbar_kws={"shrink": 0.7},
            vmin=0,
            vmax=1,
        )
        sns.heatmap(
            preds_shaped_binary,
            fmt="",
            ax=ax[1],
            cbar_kws={"shrink": 0.7},
            vmin=0,
            vmax=1,
        )

        ax[0].set_aspect("equal")
        ax[0].set_title("Prediction")
        ax[1].set_aspect("equal")
        ax[1].set_title("Prediction (binary)")
        ax[0].tick_params(axis="y", rotation=0)
        ax[1].tick_params(axis="y", rotation=0)

        fig.set_size_inches(10, 5)
        fig.suptitle(
            f"Inference Data: MSA/ {file_name.split('/')[-1]}"
        )

        buffer = BytesIO()
        fig.savefig(buffer, format="pdf")
        merger.append(buffer)

        plt.close(fig)  # close plot figure to free memory
        buffer.seek(0)
    buffer_plot = BytesIO()
    merger.write(buffer_plot)
    buffer_plot.seek(0)

    # meta=api.get_metadata()
    pdf = FPDF(orientation="L")
    pdf.add_page()
    pdf.set_font("Arial", size=12)

    pdf.cell(
        210, 10, txt=" Parameters used for inference", ln=1, align="C"
    )
    pdf.cell(60, 10, txt="hparams", align="c")
    pdf.cell(70, 10, txt="value", align="c")
    pdf.multi_cell(
        100, 10, txt="help", border=0, align="c", fill=False
    )
    pdf.line(pdf.get_x(), pdf.get_y(), pdf.get_x() + 260, pdf.get_y())
    # Maybe a best way to access this data from the DEEPaaS package? For speed
    # Might be a good idea to create an issue when requirements are clear
    predict_args = api.get_predict_args()
    for arg in set(predict_args) - set(["accept"]):
        pdf.cell(60, 10, txt=f"{arg}:", align="L")
        pdf.cell(70, 10, txt=str(default_conf[arg]), align="L")
        pdf.multi_cell(
            100,
            10,
            txt=str(predict_args[arg].metadata["description"]),
            border=0,
            align="L",
            fill=False,
        )
    # concatenate the pdf files into a buffer
    # save metadata into a buffer
    buffer_meta = BytesIO()  # st
    # Save PDF as buffer
    pdf_output = pdf.output(dest="S")
    buffer_meta.write(pdf_output.encode("latin-1"))  # 'utf-8'
    buffer_meta.seek(0)

    # merge the plots and metadata into a pdf
    pdf_merger = PdfFileMerger()
    pdf_merger.append(buffer_plot)
    pdf_merger.append(buffer_meta)
    buffer_merged = BytesIO()
    pdf_merger.write(buffer_merged)
    pdf_merger.close()
    buffer_merged.seek(0)
    return buffer_merged


def inference_XGB(
    filenames,
    backbone,
    xgboost,
    batch_size=1,
    subsampling_mode="uniform",
    distance_threshold=10.0,
    diag_shift=4,
    use_gpu=False,
    downstream_task="contact",
    original_filename=None,
):
    """_summary_

    Arguments:
        filenames -- _description_
        backbone -- _description_
        xgboost -- _description_

    Keyword Arguments:
        batch_size -- _description_ (default: {1})
        subsampling_mode -- _description_ (default: {'uniform'})
        distance_threshold -- _description_ (default: {10.0})
        diag_shift -- _description_ (default: {4})
        use_gpu -- _description_ (default: {False})

    Raises:
        ValueError: _description_

    Returns:
        _description_
    """

    if use_gpu:
        if torch.cuda.is_available():
            device = torch.device("cuda:0")
        else:
            raise ValueError("Torch cuda not available")
    else:
        device = torch.device("cpu")

    path_to_ckpt = Path(config.MODEL_CACHES["backbone"]) / backbone
    path_to_ckpt = str(path_to_ckpt) + "/final_model.ckpt"

    h_params = xgb_utils.get_checkpoint_hparams(path_to_ckpt, device)

    inference_dl = xgb_utils.create_dataloader(
        "inference",
        batch_size,
        subsampling_mode,
        distance_threshold,
        h_params,
        downstream_task,
        fasta_files=filenames,
    )
    cull_tokens = xgb_utils.get_cull_tokens(inference_dl.dataset)
    model = xgb_utils.load_backbone(
        path_to_ckpt,
        device,
        inference_dl.dataset,
        cull_tokens,
        h_params,
        downstream_task,
    )
    compute = xgb_utils.compute_attn_maps
    attn_maps, _, msa_mapping, msa_mask, _, L_mapping = compute(
        model, inference_dl, cull_tokens, diag_shift, h_params, device
    )

    # DMatrix is an internal data structure that is used by XGBoost,
    # which is optimized for both memory efficiency and training speed.
    # You can construct DMatrix from multiple different sources of data.
    inference_data = xgb.DMatrix(attn_maps, label=None)
    # lead the XGboost

    path_to_XGB = Path(config.MODEL_CACHES["xgboost"]) / xgboost
    path_to_XGB = str(path_to_XGB) + "/model_checkpoint.json"

    xgb_model = xgb.Booster(model_file=path_to_XGB)
    # predict
    preds = xgb_model.predict(
        inference_data,
        iteration_range=(0, xgb_model.best_iteration),
        strict_shape=True,
    )[:, 0]
    preds_shaped, preds_shaped_binary = save_contact_maps(
        preds, msa_mapping, msa_mask, L_mapping, original_filename
    )

    conf = {
        "input_file": original_filename,
        "backbone": backbone,
        "xgboost": xgboost,
        "batch_size": batch_size,
        "distance_threshold": distance_threshold,
        "diag_shift": diag_shift,
        "subsampling_mode": subsampling_mode,
        "use_gpu": use_gpu,
        "downstream_task": downstream_task,
    }
    buffer_out = plot_contact_maps_buffer(
        conf,
        preds,
        msa_mapping,
        msa_mask,
        L_mapping,
        [original_filename],
    )
    xgboost_metadata = json.loads(xgb_model.save_config())["learner"]

    return (
        preds,
        preds_shaped,
        preds_shaped_binary,
        buffer_out,
        xgboost_metadata,
        h_params,
        conf,
    )
