"""Utilities module for rnacontactmap API response parsers and content types.
"""
import logging
from rnacontactmap import utils

logger = logging.getLogger("__name__")


class ResponseConverter:
    def __init__(
        self,
        filename=None,
        preds=None,
        preds_shaped=None,
        preds_shaped_binary=None,
        buffer_out=None,
        xgboost_metadata=None,
        h_params=None,
        conf=None,
    ):
        self.filename = filename
        self.preds = preds
        self.preds_shaped = preds_shaped
        self.preds_shaped_binary = preds_shaped_binary
        self.buffer_out = buffer_out
        self.xgboost_metadata = xgboost_metadata
        self.h_params = h_params
        self.conf = conf
        self.logger = logging.getLogger("__name__")

    def json_response(self):
        self.logger.debug(
            "Response preds_shaped: %d", self.preds_shaped
        )
        self.logger.debug(
            "Response preds_shaped_binary: %d",
            self.preds_shaped_binary,
        )

        response = {
            "Backbone_config": self.h_params,
            "XGboost_config": self.xgboost_metadata,
            "inference_params": self.conf,
        }
        return (
            self.preds_shaped,
            utils.convert_boolean_to_binary(self.preds_shaped_binary),
            response,
        )

    def pdf_response(self):
        self.logger.debug("Response buffer_out: %d", self.buffer_out)
        self.buffer_out.name = "plot_contact.pdf"
        return self.buffer_out

    def html_response(self):
        list_dict = self.json_response()
        fig = utils.plotly_contact_maps(self.filename, list_dict)
        return fig

    def response_parsers(self):
        return {
            "application/json": self.json_response,
            "application/pdf": self.pdf_response,
            "text/html": self.html_response,
        }

    def content_types(self):
        return list(self.response_parsers().keys())


content_types = ResponseConverter().content_types()
