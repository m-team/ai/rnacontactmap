"""Configuration loader for rnacontactmap."""
import configparser
import os
import pathlib
from importlib import metadata
import logging

MODEL_NAME = os.getenv("MODEL_NAME", default="rnacontactmap")
# Get AI model metadata
API_METADATA = metadata.metadata(MODEL_NAME)

# Fix metadata for emails from pyproject parsing
_EMAILS = API_METADATA["Author-email"].split(", ")
_EMAILS = map(lambda s: s[:-1].split(" <"), _EMAILS)
API_METADATA["Author-emails"] = dict(_EMAILS)

# Fix metadata for authors from pyproject parsing
_AUTHORS = API_METADATA.get("Author", "").split(", ")
_AUTHORS = [] if _AUTHORS == [""] else _AUTHORS
_AUTHORS += API_METADATA["Author-emails"].keys()
API_METADATA["Authors"] = sorted(_AUTHORS)

# logging level across API modules can be setup via API_LOG_LEVEL,
# options: DEBUG, INFO(default), WARNING, ERROR, CRITICAL
ENV_LOG_LEVEL = os.getenv("API_LOG_LEVEL", default="INFO")
LOG_LEVEL = getattr(logging, ENV_LOG_LEVEL.upper())


homedir = os.path.dirname(os.path.normpath(os.path.dirname(__file__)))
base_dir = os.path.dirname(os.path.abspath(homedir))

BATH_DIR = os.environ.get("BATH_DIR", default=base_dir)
   
# Get configuration from user env and merge with pkg settings
SETTINGS_FILE = pathlib.Path(__file__).parent / "settings.ini"
SETTINGS_FILE = os.getenv(
    "RNACONTACTMAP_SETTINGS", default=SETTINGS_FILE
)
settings = configparser.ConfigParser()
settings.read(SETTINGS_FILE)


def resolve_path(base_dir):
    if os.path.isabs(base_dir):
        return base_dir
    else:
        return os.path.abspath(os.path.join(homedir, base_dir))


# base_dir = resolve_path(base_dir)


try:  # Configure input files for testing and possible training
    DATA_PATH = os.path.join(BATH_DIR, settings["data"]["path"])
    DATA_PATH = os.getenv("DATA_PATH", default=DATA_PATH)
   
    # Selbstaufsicht requires currently the setup of DATA_PATH env variable
    os.environ["DATA_PATH"] = DATA_PATH
except KeyError as err:
    raise RuntimeError(
        "Undefined configuration for [data]path"
    ) from err


try:
    remote_path = settings["remote"]["path"]
    MODEL_REMOTES = {
        "backbone": os.path.join(remote_path, "backbone"),
        "xgboost": os.path.join(remote_path, "xgboost"),
    }

    for key, value in MODEL_REMOTES.items():
        os.environ[key] = value

except KeyError as err:
    raise RuntimeError(
        "Undefined configuration for [remote_backbone] path"
    ) from err


try:  # Local path for caching backbone sub/models
    MODEL_CACHES = {}
    BACKBONE_PATH = os.path.join(
        BATH_DIR, settings["backbone"]["path"]
    )
    BACKBONE_PATH = os.getenv("BACKBONE_PATH", default=BACKBONE_PATH)

    MODEL_CACHES["backbone"] = BACKBONE_PATH
    
except KeyError as err:
    raise RuntimeError(
        "Undefined configuration for [backbone]path"
    ) from err


try:  # Local path for caching xgboost sub/models
    XGBOOST_PATH = os.path.join(base_dir, settings["xgboost"]["path"])
    XGBOOST_PATH = os.getenv("XGBOOST_PATH", default=XGBOOST_PATH)
    
    MODEL_CACHES["xgboost"] = XGBOOST_PATH
    MODEL_CACHES= os.getenv("MODEL_CACHES", default=MODEL_CACHES)
except KeyError as err:
    raise RuntimeError(
        "Undefined configuration for [xgboost]path"
    ) from err
